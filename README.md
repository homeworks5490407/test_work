# test_work
Вадим, [18.02.2024 21:17]
ч#################
# Пре-реквизиты #
#################
На ВМ mkbtaxm-08 созданы все необходимые директории:
distr #Папка с необходимым софтом
hints #Папка с подсказками
ilshat: #Рабочая папка Ильшата
     postgres (https://src.cinimex.ru/VKTAXCM/ci/infra)
     ansible (https://src.cinimex.ru/MKBTAXM/ci/infra)
        ansible
            inventory
            roles
     infra
        clickhouse-ilshat
        exporter-ilshat
        kafka-ilshat
        minio-ilshat
        promtail-ilshat
        redis-ilshat
     teraform (https://src.cinimex.ru/MKBTAXM/ci/infra)
         modules
         terraform 
volodya: #Рабочая папка Владимира
     postgres (https://src.cinimex.ru/VKTAXCM/ci/infra)
     ansible (https://src.cinimex.ru/MKBTAXM/ci/infra)
        ansible
            inventory
            roles
     infra
        clickhouse-volodya
        exporter-volodya
        kafka-volodya
        minio-volodya
        promtail-volodya
        redis-volodya
     teraform (https://src.cinimex.ru/MKBTAXM/ci/infra)
         modules
         terraform
#############
# ЗАДАЧА 1 #
#############
1. Необходимо (используя директорию teraform):
    - написать terraform
    - выполнить plan конфигурации
    - запустить создание ВМ (https://vcenter.cmx.ru/ui/app/folder;nav=v/urn:vmomi:Folder:group-v1184686:52ce2345-a4a7-418a-b378-3957c2bd39d1/summary)
Данные для Владмира:
- host = mkbtaxm-11
- ip = 192.168.26.??
- cpu = 4
- memory = 8gb
Данные для Ильшата:
- host = mkbtaxm-10
- ip = 192.168.26.??
- cpu = 4
- memory = 8gb
#############
# ЗАДАЧА 2 #
#############
2. Настроить свою ВМ из "ЗАДАЧА 1" (используя директорию ansible):
- lvm
- сети (firewall не выключаем)
- selinux
- docker
#############
# ЗАДАЧА 3 #
#############
3. На хосте mkbtaxm-08 поднять:
- postgres (исользуя директорию postgres)
    - создать пользователя с паролем
    - создать БД
    - создать гранты
- clickhouse (исользуя директорию infra)
    - создать пользователя с паролем
    - создать БД 
- minio (исользуя директорию infra)
    - создать пользователя с паролем
    - создать bucket
- kafka (исользуя директорию infra)
- schema-registry (исользуя директорию infra)
- redis (исользуя директорию infra)
#############
# ЗАДАЧА 4 #
#############
4. На хосте mkbtaxm-04:
- подготовить конфигурационные файлы для "ВНМ" 
- выполнить установку релизу через install-release.py
- перенести установку на рабочий хост через "Makefile"
- выполнить запуск приложения через "Makefile"
- завершить установку приложения 
Данные для Владмира:
- name = voloday
- tag = 1.1.8
- host = mkbtaxm-11
- keycloak = https://taxmon-keycloak.apps.ocp.cmx.ru/auth/admin/master/console/#/RNCB/clients
    - realm = RNCB
Данные для Ильшата:
- name = ilshat
- tag = 1.1.7
- host = mkbtaxm-10
- keycloak = https://taxmon-keycloak.apps.ocp.cmx.ru/auth/admin/master/console/#/RNCB/clients
    - realm = RNCB
#############
# ЗАДАЧА 5 #
#############
5. Подключить к мониторингу ВМ
- использовать node-exporter (исользуя директорию postgres)
- использовать promtail (исользуя директорию postgres)
- проверить, что ВМ появилась в мониторинге
- проверить, что ВМ пишутся логи в Loki
    - приложение ВНМ
    - postgres 
    - clickhouse 
    - kafka
########
# ИТОГ #
########
- На хосте mkbtaxm-08 полностью рабочие конфигруации:
    - terraform
    - ansible
    - infra
    - postgres
- На хосте mkbtaxm-1{0,1} полностью рабочие конфигруации:
    - приложения "ВНМ"
- Имеется доступ к ui-"ВНМ" через браузер
    - через ldap
    - через system?login=true
- ВМ видно в системе мониторинга и собираются данные 
- Собираются логи в Loki и отображаются в grafana


##### в кратце так
terrafrom поднимаеш машинки
ansible наполняешь эти ВМ ПО и СОФТОМ/ далее делаешь все настройки


заваодишь все в мониторинг/собираешь логи


выводишь в графану все


и в cicd запихиваешь ansible


